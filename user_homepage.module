<?php

/**
 * @file
 * User Homepage module.
 *
 * Allows users to choose their own homepage.
 */

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_user_login().
 */
function user_homepage_user_login($account) {
  // Redirect only if there's no 'destination' param in URL.
  if (empty(Drupal::request()->query->get('destination'))) {
    $userHomepageManager = Drupal::service('user_homepage.manager');
    $userHomepageManager->resolveUserRedirection($account);
  }
}

/**
 * Implements hook_help().
 */
function user_homepage_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Help page for the user_homepage module.
    case 'help.page.user_homepage':
      $link_options = [
        'attributes' => ['target' => '_blank'],
        'fragment' => 'module-user_homepage',
      ];

      $user_homepage_permission = Link::createFromRoute(t('Configure own homepage'), 'user.admin_permissions', [], $link_options)->toString();

      $help_text = '<p>' . t("The User Homepage module lets users with the 
        \"!user_homepage_permission'\" permission choose a specific page of the 
        site as their homepage. Users with a homepage will be redirected to this 
        page upon successful login on the site.",
          [
            '!user_homepage_permission' => $user_homepage_permission,
          ]) . '</p>';
      $help_text .= '<p>' . t("The module provides two blocks. One of them 
        allows users to save the current page as their homepage, and the other 
        allows them to reset their homepage so that they are no longer 
        redirected after login in.") . '</p>';
      $help_text .= '<p>' . t("Note that if there is a 'destination' parameter 
        in the login request, the homepage redirect is not triggered, and the 
        user is taken to the path specified in the 'destination' parameter.") . '</p>';

      $block_layout_page = Link::createFromRoute(t('Block layout'), 'block.admin_display')->toString();
      $help_text .= t('The blocks can be configured and placed in any theme
        region from the !block_layout_page page.',
        [
          '!block_layout_page' => $block_layout_page,
        ]);
      return $help_text;
  }
}
